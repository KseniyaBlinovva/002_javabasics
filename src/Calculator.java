import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.EmptyStackException;
import java.util.Stack;

public class Calculator {
    //преобоазование в постфикс
    public static String getExpression(String input) {
        StringBuilder output = new StringBuilder();
        Stack<Character> s = new Stack<>();
        char[] ch = input.toCharArray();
        for (int i = 0; i < ch.length; i++) {
            if (Operand(ch[i]))
                continue;
            if (!Operator(ch[i])) {
                while (!Operator(ch[i]) && !Operand(ch[i])) {
                    output.append(ch[i]);
                    i++;

                    if (i == ch.length)
                        break;
                }
                output.append(" ");
                i--;
            } else {
                if (ch[i] == '(')
                    s.push(ch[i]);
                else if (ch[i] == ')') {
                    output.append(s.pop().toString() + " ");
                    s.pop();
                } else {
                    if (!s.isEmpty())
                        if (getPriority(ch[i]) <= getPriority(s.peek()))
                            output.append(s.pop().toString() + ' ');
                    s.push(ch[i]);
                }
            }
        }

        while (!s.isEmpty())
            output.append(s.pop().toString() + " ");
        return output.toString();
    }

    public static boolean valid(String input) {
        Stack<Character> s = new Stack<>();
        char[] ch = input.toCharArray();
        try {
            for (int i = 0; i < ch.length; i++) {
                if (ch[i] == '(') {
                    s.push(ch[i]);
                }
                if (ch[i] == ')') {
                    s.pop();
                }
            }
        } catch (EmptyStackException e) {
            return false;
        }
        return s.isEmpty();
    }

    public static double counting(String input) {
        double result = 0;
        Stack<Double> stack = new Stack<>();
        char[] ch = input.toCharArray();
        for (int i = 0; i < ch.length; i++) {
            if (!Operator(ch[i]) && !Operand(ch[i])) {
                StringBuilder a = new StringBuilder();
                while (!Operator(ch[i]) && !Operand(ch[i])) {
                    a.append(ch[i]);
                    i++;
                    if (i == ch.length)
                        break;
                }
                stack.push(Double.parseDouble(a.toString()));
            } else if (Operator(ch[i])) {
                double a = stack.pop();
                double b = stack.pop();
                switch (ch[i]) {
                    case '+':
                        result = b + a;
                        break;
                    case '-':
                        result = b - a;
                        break;
                    case '*':
                        result = b * a;
                        break;
                    case '/':
                        result = b / a;
                        break;
                }
                stack.push(result);
            }
        }
        return result;
    }

    public static int getPriority(char s) {
        return s == '+' || s == '-' ? 1 : 2;
    }

    //возвращает true, если проверяемый символ оператор
    public static boolean Operator(char s) {
        if ("+-*/()".indexOf(s) != -1)
            return true;
        else
            return false;
    }

    //возвращает true, если проверяемый символ - разделитель ("пробел" или "равно")
    public static boolean Operand(char s) {
        if ((" =".indexOf(s) != -1))
            return true;
        return false;
    }

    public static void calculate(String input) {
        if (!valid(input))
            System.out.println("Выражение введено некоректно");
        else {
            String output = getExpression(input);
            double result = counting(output);
            System.out.println(result);
        }
    }

    public static void main(String[] args) throws IOException {
        System.out.println("Введите выражение. Например 2 + 4 * 2");
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        String s = br.readLine();
        calculate(s);
    }


}